import { Injectable } from '@angular/core';
import { Partidos } from './products.model';

@Injectable({
  providedIn: 'root'
})
export class tab2service {
  
  private Partidos:Partidos [] = [
    {
      Equipos: "LDA vs Saprissa",
      IdPartido:111,
      Goles:2,
      TarjetasAmarillas: 2,
      TarjetasRojas: 2,
      Faltas: 1,
      HoraFinalizacion: "11:55pm"
    },
    
  ];
  constructor() { }
  getAll(){
    return [...this.Partidos];
  }
  getProduct(IdPartido: number){
    return {
      ...this.Partidos.find(
        partidos => {
          return partidos.IdPartido === IdPartido;
        }
      )
    };
  }
 deletePartido(IdPartido: number){
    this.Partidos = this.Partidos.filter(
      Partidos => {
        return Partidos.IdPartido !== IdPartido;
      }
    );
  }
  AgregarPartido(
    pEquipos:string,
    pIdPartido: number,
    pGoles: number,
    pTarjetasAmarillas: number,
    pTarjetasRojas: number,
    pFaltas: number,
    pHoraFinalizacion: string){

      const partidos: Partidos = {
        Equipos:pEquipos,
        IdPartido:pIdPartido,
        Goles: pGoles,
        TarjetasAmarillas: pTarjetasAmarillas,
        TarjetasRojas: pTarjetasRojas,
        Faltas: pFaltas,
        HoraFinalizacion: pHoraFinalizacion
      }
      this.Partidos.push(partidos);
      
  }
}
