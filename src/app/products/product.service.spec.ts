import { TestBed } from '@angular/core/testing';

import { tab2service } from './product.service';

describe('ProductService', () => {
  let service: tab2service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(tab2service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
