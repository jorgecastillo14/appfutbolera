
export interface Partidos{
    Equipos: string;
    IdPartido: number;
    Goles: number;
    TarjetasAmarillas: number;
    TarjetasRojas: number;
    Faltas: number;
    HoraFinalizacion: string;
    
  }
  