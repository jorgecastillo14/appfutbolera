import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { partidospage } from './products.page';

describe('partidospage', () => {
  let component: partidospage;
  let fixture: ComponentFixture<partidospage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ partidospage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(partidospage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
