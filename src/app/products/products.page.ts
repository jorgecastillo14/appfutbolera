import { Component, Input, OnInit } from '@angular/core';
import { tab2service } from './product.service';
import { Partidos } from './products.model';

@Component({
  selector: 'page-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class partidospage implements OnInit {
  @Input() Partido: string;
  partidos: Partidos[];
  constructor(private productServices: tab2service) { }

  ngOnInit() {

  }
  ionViewWillEnter(){
    console.log("Se obtuvo la lista");
    this.partidos = this.productServices.getAll();
  }
}
