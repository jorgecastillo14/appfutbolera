import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { tab2service } from '../product.service';
import { Partidos } from '../products.model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  partidos: Partidos;
  constructor(
    private activeRouter: ActivatedRoute,
    private productService: tab2service,
    private router: Router,
    private alertController: AlertController
  ) { }
  
  ngOnInit() {
    this.activeRouter.paramMap.subscribe(
      paramMap => {
        if(!paramMap.has('productId')){
          return;
        }
        const productId =  parseInt( paramMap.get('productId'));
        this.partidos = this.productService.getProduct(productId);
      }
    );
  }

  deletePartido(){
    this.alertController.create({
      header: "Borrar Partido",
      message: "Esta seguro que desea borrar este Partido?",
      buttons:[
        {
          text:"No",
          role: 'no'
        },
        {
          text: 'Borrar',
          handler: () => {
            this.productService.deletePartido(this.partidos.IdPartido);
            this.router.navigate(['./Partidos']);
          }
        } 
      ]
    })
    .then(
      alertEl => {
        alertEl.present();
      }
    );
    
  }
}
