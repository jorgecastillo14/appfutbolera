import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { tab2service } from '../product.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  formAgregarPartido: FormGroup;
  constructor(
    private servicetab2: tab2service,
    private router: Router
    ) { }

  
    ngOnInit() {
      this.formAgregarPartido = new FormGroup({
        Equipos: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required]
          }
        ),
        IdPartido: new FormControl(
          1,
          {
            updateOn: 'blur',
            validators: [Validators.required, Validators.min(1)]
          }
        ),
        Goles: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators: [Validators.required, Validators.min(1)]
          }
        ),
        TarjetasAmarillas: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.min(1)]
          }
        ),
        TarjetasRojas: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.min(1)]
          }
        ),
        Faltas: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.min(1)]
          }
        ),
        HoraFinalizacion: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.min(1)]
          }
        ),
        
      });
    }
    AgregarPartido(){
      if(!this.formAgregarPartido.valid){
        return;
      }
      this.servicetab2.AgregarPartido(
        this.formAgregarPartido.value.pEquipos,
        this.formAgregarPartido.value.pIdPartido,
        this.formAgregarPartido.value.pGoles,
        this.formAgregarPartido.value.pTarjetasAmarillas, 
        this.formAgregarPartido.value.pTarjetasRojas,
        this.formAgregarPartido.value.pFaltas,
        this.formAgregarPartido.value.pHoraFinalizacion
       
      );
      this.formAgregarPartido.reset();
      this.router.navigate(['/products']);
    }
  
  
  }